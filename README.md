# Currency Converter Java Application
This is a simple java command line currency converter that expects/gets three input parameters: 
```
	- amount 
	- from currency
	- to currency 
```
The result will be calculated by the latest currency rate and displayed as: 
```
<amount> <from currency> = xx.xx <to currency>
```

For example by giving inputs as
```
12 EUR USD
```
the response will be: 
```
12 EUR = 13.36 USD
```

Currency rates are fetched by using this API https://api.exchangeratesapi.io/latest?base=EUR

## To run this project

1. Clone this repository. $ git clone https://jufni_rizada@bitbucket.org/jufni_rizada/jufni-java-test.git
2. Open Eclipse and import project as "Existing Maven Project".
3. Right-click project, click "Maven" -> then click "Update Project". This is to update Maven dependencies.
4. Build project.
5. Explore project and right-click on the CurrencyConverter.java.
6. Click "Run As" -> "Run Configurations", then double click "Java Application".
7. In Arguments tab, input test arguments (eg. 12 USD EUR) in the "Program arguments" text area. 
8. View Console to check the conversion.

## Pre-requisite
1. Java 8 (Source code was built and tested on Java 8 SDK.)


